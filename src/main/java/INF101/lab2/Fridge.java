package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;
    List<FridgeItem> itemsInFridge;
    List<FridgeItem> expiredFood;

    public Fridge() {
        this.itemsInFridge = new ArrayList<>();
        this.expiredFood = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge.size() < max_size) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!itemsInFridge.contains(item)) {
            throw new NoSuchElementException();
        } 
        else {
            itemsInFridge.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        itemsInFridge.removeIf(FridgeItem::hasExpired);
        
        return expiredFood;
    }

}
